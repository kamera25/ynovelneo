﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public enum COMMAND
{
	NONE 				= 0,
	WAIT_CLICK 			= 1,
	NEWPAGE 			= 2,
	WAIT 				= 3,
	LOAD_GRAPHIC 		= 4,
	FILL_GRAPHIC 		= 5,
	PUT_CHARACTOR 		= 6,
	REMOVE_CHARA 		= 7,
	REMOVE_ALL_CHARA 	= 8,
	PUT_WINDOW 			= 9,
	REMOVE_WINDOW 		= 10,
	//CHANGE_LINKWINDOW = 10,
	//RESET_LINKWINDOW = 11,
	PLAY_BGM 			= 11,
	STOP_BGM 			= 12,
	FADEOUT_BGM 		= 13,
	PLAY_SE 			= 14,
	STOP_SE 			= 15,
	SET_COUNTER			= 16,
	ADD_COUNTER			= 17,
	RANDOM_COUNTER		= 18,
	ADD_RAND_COUNTER	= 19,
	PUT_MESSAGE			= 100,
	PUT_MESSAGE_NL		= 101,
	RUN_MENU_LINK		= 200,
	RUN_AUTO_LINK		= 201,
	RUN_ENDING			= 202
};

public enum HORIZONTAL
{
	RIGHT	= 0,
	CENTER	= 1,
	LEFT	= 2
}

public enum VERTICAL
{
	TOP		= 0,
	MIDDLE	= 1,
	BOTTOM	= 2
}

public enum LINKBEHAVIOR
{
	MENU = 0,
	AUTO = 1,
	ENDING = 2
}

public class Charactor
{
	public string name;
	public VERTICAL vertical;
	public HORIZONTAL horizontal;
	public Image image;
}

[System.Serializable]
public class YNVScript
{
	public COMMAND cmd;
	public string message = "";
	public string message2 = "";
	public List<int> parameta = new List<int>();
}


// アイテムのノードクラス
[System.Serializable]
public class YNV_Item 
{
	public string iName = "新規アイテム";
	public Vector2 pos = Vector2.zero;
	public Vector2 size = new Vector2 ( 160, 50);
	public bool push = false;
	public bool selected = false;
	public string script = "";
	
	public LINKBEHAVIOR linkBehavior = LINKBEHAVIOR.MENU;
	public List<YNV_Link> link = new List<YNV_Link>(); 
	
	// 分岐が存在するか.
	public bool existBranch()
	{
		return link.Count > 0;
	}
}

// リンクのデータクラス
[System.Serializable]
public class YNV_Link
{
	public bool enabled = true;
	public string text = "";
	public bool useCondition = false;
	public YNV_Condition condition = null;

	public string linkedItemName = "";
	
	[System.NonSerialized]
	public YNV_Item linkedItem = null;

	public void SerializedProcess()
	{
		if (linkedItem == null) 
		{
			linkedItemName = "";
		}
		else 
		{
			linkedItemName = linkedItem.iName;
		}
	}

	public void DeserializedProcess( List<YNV_Item> itemList)
	{
		if (linkedItemName == "") 
		{
			linkedItem = null;
		}
		else 
		{
			linkedItem = itemList
						.Where( x => x.iName == linkedItemName)
						.FirstOrDefault();
		}

	}
}

// カウンタのデータクラス
[System.Serializable]
public class YNV_Counter
{
	public string name = "新規カウンタ";
	public int defaultNum = 0;
	public int max = 100;
	public int min = 0;
	public bool carryOver = false;
}

// 条件のデータクラス
[System.Serializable]
public class YNV_Condition
{
	public YNV_Counter counter = null;
	public int moreThanNum = 0;
}