﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif


[System.Serializable]
public class YNV_SaveData : ScriptableObject
{
	[SerializeField]
	public List<YNV_Item> itemList;
	
	[SerializeField]
	public List<YNV_Counter> counterList;
	
	////////////////////////////////////
	
	void OnEnable()
	{
		if (itemList == null)
			itemList = new List<YNV_Item> ();
		
		if (counterList == null)
			counterList = new List<YNV_Counter> ();
	}
	
	// この保存する.
	public static void SaveData( List<YNV_Item> itemL, List<YNV_Counter> counterL)
	{
		#if UNITY_EDITOR
		string dataPath = "Assets/YNovelNeo/Data/Resources/" + typeof(YNV_SaveData) + ".asset";
		AssetDatabase.DeleteAsset (dataPath);
		
		YNV_SaveData data = YNV_SaveData.CreateInstance <YNV_SaveData>();
		
		data.itemList = itemL;
		data.counterList = counterL;
		data.FinalizeSavingProcess ();
		
		AssetDatabase.CreateAsset ( data, dataPath);
		EditorUtility.SetDirty (data);
		AssetDatabase.SaveAssets ();
		
		data.hideFlags = HideFlags.NotEditable;
		
		#endif
	}
	
	// このデータを保存する.
	public static YNV_SaveData LoadData( string path)
	{

		#if UNITY_EDITOR
		string dataPath = "Assets/YNovelNeo/Data/Resources/" + typeof(YNV_SaveData) + ".asset";
		YNV_SaveData saveData = AssetDatabase.LoadAssetAtPath<YNV_SaveData> (dataPath); 
		#else
		YNV_SaveData saveData = Resources.Load<YNV_SaveData>( typeof(YNV_SaveData).ToString());
		#endif
		
		// YNV_SaveData( YNV_Item と YNV_Counter の集合クラス )のScriptableObjectをロード
		
		if (saveData == null) 
		{
			Debug.Log ("No exist saved script data.");
		}
		else 
		{
			saveData.InitializeLoadProcess ();
		}

		saveData.hideFlags = HideFlags.NotEditable;

		return saveData;
	}
	
	
	// 保存前に実行する.
	public void FinalizeSavingProcess()
	{
		foreach (YNV_Item item in itemList) 
		{
			foreach( YNV_Link link in item.link)
			{
				link.SerializedProcess();
			}
		}
	}
	
	// ロード時に実行する. リンクの再結合処理など.
	public void InitializeLoadProcess()
	{
		foreach (YNV_Item item in itemList) 
		{
			foreach( YNV_Link link in item.link)
			{
				link.DeserializedProcess( itemList);
			}
		}
	}
	
}