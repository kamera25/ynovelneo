﻿using System.Collections.Generic;
using UnityEngine;

// 実行時のカウンタを制御するマネージャクラスです.
public class CounterManager : MonoBehaviour 
{
	public Dictionary<string, int> nowCounterDictionary = 
		new Dictionary<string, int>();

	public Dictionary<string, YNV_Counter> counterDataDictionary = 
		new Dictionary<string, YNV_Counter>();


	public void Initialize( List<YNV_Counter> clist)
	{

		// YNovelRuntime から カウンタリストを取得
		foreach (YNV_Counter counter in clist) 
		{
			nowCounterDictionary[counter.name] = counter.defaultNum;

			counterDataDictionary[counter.name] = counter;
		}
	}

	// 数をカウンタに加算するときに利用
	public void AddVar( string key, int num)
	{
		if (!nowCounterDictionary.ContainsKey (key)) 
		{
			Debug.LogError( "エラー : " + key + "という名前のカウンタは存在しません!");
			return;
		}

		int min = counterDataDictionary [key].min;
		int max = counterDataDictionary [key].max;

		nowCounterDictionary [key] = Mathf.Clamp ( num + nowCounterDictionary [key], min, max);
	}

	// 数をカウンタにセットするときに利用.
	public void SetVar( string key, int num)
	{
		if (!nowCounterDictionary.ContainsKey (key)) 
		{
			Debug.LogError( "エラー : " + key + "という名前のカウンタは存在しません!");
			return;
		}
		
		int min = counterDataDictionary [key].min;
		int max = counterDataDictionary [key].max;
		
		nowCounterDictionary [key] = Mathf.Clamp ( num, min, max);
	}

	// ランダムな数を加算するときに利用.
	public void AddRandVar( string key, int minNum, int maxNum)
	{
		if (!nowCounterDictionary.ContainsKey (key)) 
		{
			Debug.LogError( "エラー : " + key + "という名前のカウンタは存在しません!");
			return;
		}

		int rand = Random.Range (minNum, maxNum);
		AddVar (key, rand);
	}

	// ランダムな数をセットするときに利用
	public void SetRandVar( string key, int minNum, int maxNum)
	{
		if (!nowCounterDictionary.ContainsKey (key)) 
		{
			Debug.LogError( "エラー : " + key + "という名前のカウンタは存在しません!");
			return;
		}

		int rand = Random.Range (minNum, maxNum);
		SetVar (key, rand);
	}
}
