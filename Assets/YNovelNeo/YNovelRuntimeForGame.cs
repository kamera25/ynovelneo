﻿using UnityEngine;
using System.Collections;

public class YNovelRuntimeForGame : YNovelRuntime 
{
	public YNV_SaveData UseYNVData ; 

	protected override void Start ()
	{
		base.Start ();

		isGame = true;
	}

	protected override void ScriptInit ()
	{
		string path = "";

		if (UseYNVData != null)
			path = UseYNVData.name;

		saveData = YNV_SaveData.LoadData (path);
		ParseScript ();
	}

	protected override void RunEnding ()
	{
		Destroy (this.gameObject);
		GameObject.FindWithTag ("GameController").SendMessage ("EndNovelProcess");
	}
}
