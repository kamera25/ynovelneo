﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class YNovelRuntime : MonoBehaviour 
{
	List<YNVScript> scriptList = new List<YNVScript>();
	int textNum = 0;
	int index = 0;
	float time = 0F;
	float putTime = 0.01F;
	Text screenText = null;
	YNV_Item nowItem = null;

	protected YNV_SaveData saveData = null;
	protected bool isGame = false;

	Image backgroundFW = null;
	Image backgroundBK = null;

	CounterManager counterManager = null;
	AudioSource audioSource = null;
	GameObject buttonsCanvas = null;

	List<Charactor> CharaList = new List<Charactor>();// キャラクタデータを入れる構造体配列

	void Awake()
	{
		this.name = "YNVController";
	}

	// Use this for initialization
	protected virtual void Start () 
	{
		InitilaizeGame ();

		counterManager = gameObject.AddComponent<CounterManager> ();
		counterManager.Initialize (saveData.counterList);

		audioSource = gameObject.AddComponent<AudioSource> ();
		audioSource.loop = true;

	}

	void InitilaizeGame()
	{
		Camera.main.backgroundColor = Color.black;
		
		// UIオブジェクトを取得
		GameObject canvas = Instantiate<GameObject>(Resources.Load("Prefabs/YNV_UICanvas") as GameObject);
		canvas.transform.SetParent (this.transform);

		screenText = canvas.transform.FindChild("Text").GetComponent<Text>();
		backgroundBK = canvas.transform.FindChild ("Background").GetComponent<Image> ();
		backgroundFW = canvas.transform.FindChild ("Background_FW").GetComponent<Image>();

		ScriptInit ();
	}

	protected virtual void ScriptInit()
	{
		saveData = YNV_SaveData.LoadData ("");
		ParseScript ();
	}

	protected void ParseScript()
	{
		// 最初のItemのデータをパース.
		nowItem = saveData.itemList [0];
		ScriptParser ( nowItem.script);
		AddLinkDataToScript (nowItem.linkBehavior);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ( !(index < scriptList.Count)) 
		{
			return;
		}


		// 現在のスクリプトがどこまで行っているかチェック
		YNVScript sc = scriptList [index];
		string path = "";
		switch ( sc.cmd) 
		{
			case COMMAND.NEWPAGE:
				screenText.text = "";
				NextProc();
			break;
			case COMMAND.PUT_MESSAGE:
				if( putTime < time)
				{
					textNum++;
					if( textNum < sc.message.Length + 1)
					{
						screenText.text = screenText.text + sc.message[textNum-1];
						time = 0F;
					}
					else
					{
						NextProc();
					}
				}
			break;
			case COMMAND.LOAD_GRAPHIC:
				BackgroundFadeIn( sc.message);
			break;
			case COMMAND.PUT_CHARACTOR:
				PutCharactor( sc.message, sc.message2, sc.parameta[0], sc.parameta[1]);
			break;
			case COMMAND.REMOVE_CHARA:
				RemoveCharactor( sc.message);
				NextProc();
			break;
			case COMMAND.REMOVE_ALL_CHARA:
				RemoveAllCharactor();
				NextProc();
			break;
			case COMMAND.WAIT:
				if( (float)sc.parameta[0] * 0.001F < time)
				{
					NextProc();
				}
			break;
			case COMMAND.PLAY_BGM:
				path = ConvertADBPathToResources( sc.message);
				audioSource.clip = Resources.Load<AudioClip>( path);
				audioSource.Play();
				NextProc();
			break;
			case COMMAND.STOP_BGM:
				audioSource.Stop();
			break;
			case COMMAND.FADEOUT_BGM:
				float waitTime = (float)sc.parameta[0] * 0.001F;
				if( waitTime < time)
				{	
					audioSource.Stop();
					audioSource.volume = 1F;
					NextProc();
				}
				else
				{
					audioSource.volume = 1F - ( time / waitTime);
				}
			break;
			case COMMAND.PLAY_SE:
				path = ConvertADBPathToResources( sc.message);
				audioSource.PlayOneShot( Resources.Load<AudioClip>(path));
			    NextProc();
			break;
			case COMMAND.SET_COUNTER:
				counterManager.SetVar( sc.message, sc.parameta[0]);
				NextProc();
			break;
			case COMMAND.ADD_COUNTER:
				counterManager.AddVar( sc.message, sc.parameta[0]);
				NextProc();
			break;
			case COMMAND.RANDOM_COUNTER:
				counterManager.SetRandVar( sc.message, sc.parameta[0], sc.parameta[1]);
				NextProc();
			break;
			case COMMAND.ADD_RAND_COUNTER:
				counterManager.AddRandVar(sc.message, sc.parameta[0], sc.parameta[1]);
				NextProc();
			break;       
			case COMMAND.RUN_AUTO_LINK:
				RunAutoLink();
				screenText.text = "";
				NextProc();
			break;
			case COMMAND.RUN_MENU_LINK:
				RunMenuLink();
				screenText.text = "";
				NextProc();
			break;
			case COMMAND.RUN_ENDING:
				RunEnding();
				screenText.text = "";
				NextProc();
			break;
		}

		time += Time.deltaTime;
	}

	void NextProc()
	{
		textNum = 0;
		time = 0F;
		index++;
	}

	
	void ScriptParser( string script)
	{

		string[] splitScript = script.Split('\n');


		foreach (string src in splitScript) 
		{
			YNVScript ynvScript = new YNVScript();

			if( src.StartsWith(".NextPage"))
			{
				ynvScript.cmd = COMMAND.NEWPAGE;
			}
			else if( src.StartsWith(".GraphLoad "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.LOAD_GRAPHIC;
				
				ynvScript.message = sentence[1].Replace("\"", "");
			}
			else if(src.StartsWith(".CharSet "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.PUT_CHARACTOR;
				
				ynvScript.message = sentence[1].Replace("\"", "");
				ynvScript.message2 = sentence[2].Replace("\"", "");
				
				int s3 = (int) Enum.Parse( typeof(VERTICAL), sentence[3]);
				int s4 = (int) Enum.Parse( typeof(HORIZONTAL), sentence[4]);
				ynvScript.parameta.Add( s3);
				ynvScript.parameta.Add( s4);
			}
			else if( src.StartsWith(".CharDelete "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.REMOVE_CHARA;
				
				ynvScript.message = sentence[1].Replace("\"", "");
			}
			else if( src.StartsWith(".CharClear"))
			{
				ynvScript.cmd = COMMAND.REMOVE_ALL_CHARA;
			}
			else if( src.StartsWith(".BGMPlay "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.PLAY_BGM;

				ynvScript.message = sentence[1].Replace("\"", "");
			}
			else if( src.StartsWith(".BGMFadeout "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.FADEOUT_BGM;

				ynvScript.parameta.Add( int.Parse( sentence[1]));
			}
			else if( src.StartsWith(".SoundPlay "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.PLAY_SE;
				
				ynvScript.message = sentence[1].Replace("\"", "");
			}
			else if( src.StartsWith(".Wait "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.WAIT;
				ynvScript.parameta.Add( int.Parse( sentence[1]));
			}
			else if( src.StartsWith(".CounterSet "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.SET_COUNTER;

				ynvScript.message = sentence[1].Replace("\"", "");
				ynvScript.parameta.Add( int.Parse( sentence[2]));
			}
			else if( src.StartsWith(".CounterPlus "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.ADD_COUNTER;
				
				ynvScript.message = sentence[1].Replace("\"", "");
				ynvScript.parameta.Add( int.Parse( sentence[2]));
			}
			else if( src.StartsWith(".CounterRndSet "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.RANDOM_COUNTER;
				
				ynvScript.message = sentence[1].Replace("\"", "");
				ynvScript.parameta.Add( int.Parse( sentence[2]));
				ynvScript.parameta.Add( int.Parse( sentence[3]));
			}
			else if( src.StartsWith(".CounterRndPlus "))
			{
				string[] sentence = src.Split(' ');
				ynvScript.cmd = COMMAND.ADD_RAND_COUNTER;
				
				ynvScript.message = sentence[1].Replace("\"", "");
				ynvScript.parameta.Add( int.Parse( sentence[2]));
				ynvScript.parameta.Add( int.Parse( sentence[3]));
			}
			else
			{
				// 「@」が見つからなければ
				if( src.IndexOf('@') == -1)
				{
					ynvScript.cmd = COMMAND.PUT_MESSAGE;
					ynvScript.message = src + '\n';
				}
				else // 「@」が見つかったら
				{
					string[] srcs = src.Split('@');

					foreach( string s in srcs)
					{

						if( s == "")
						{
							break;
						}


						// メッセージの挿入
						ynvScript.cmd = COMMAND.PUT_MESSAGE;
						ynvScript.message = s + '\n';
						scriptList.Add( ynvScript);

						// クリックウェイトの挿入
						YNVScript ynvScriptChild = new YNVScript();
						ynvScriptChild.cmd = COMMAND.WAIT_CLICK;
						scriptList.Add(ynvScriptChild);
					}

					continue;
				}

			}

			scriptList.Add(ynvScript);
		}

	}

	// 自動リンクで飛ぶ先を選定する.
	void RunAutoLink()
	{

		foreach( YNV_Link link in nowItem.link)
		{
			if( ChkAvailableLink( link))
			{
				// 次に飛ぶリンクの決定と関数の脱出.
				nowItem = link.linkedItem;
				ScriptParser ( nowItem.script);
				AddLinkDataToScript (nowItem.linkBehavior);

				return;
			}
		}
	}

	// メニューリンクを表示する.
	void RunMenuLink()
	{
		// ボタンを置くためのキャンバス
		buttonsCanvas = Instantiate<GameObject>(Resources.Load("Prefabs/YNV_MenuLinkCanvas") as GameObject);
		Transform LinkList = buttonsCanvas.transform.FindChild ("LinkListButton");

		int i = 1;

		foreach (YNV_Link link in nowItem.link) 
		{
			if( ChkAvailableLink( link))
			{
				// ボタンのロード.
				GameObject button = Instantiate<GameObject>(Resources.Load("Prefabs/LinkButton") as GameObject);
				SendMessageAsButton sendMes = button.GetComponent<SendMessageAsButton>();
				Text text = button.transform.FindChild("Text").GetComponent<Text>();

				sendMes.option = i.ToString();
				text.text = i.ToString() + " : " + link.text;
				button.transform.SetParent( LinkList, false);

				i++;
			}
		}

	}

	// エンディング
	protected virtual void RunEnding()
	{

	}

	// リンク先に飛ぶことor選択できるか.
	bool ChkAvailableLink( YNV_Link link)
	{
		// 有効？
		if( !link.enabled)
		{
			return false;
		}
		
		// 条件文あり？
		if( link.useCondition)
		{
			// 条件分を満たしてない？
			if( !(counterManager.nowCounterDictionary[link.condition.counter.name] >= link.condition.moreThanNum))
			{
				return false;
			}
		}

		return true;
	}


	void AddLinkDataToScript( LINKBEHAVIOR linkBhv)
	{
		// 最後にリンク設定から分岐情報を書き込み
		YNVScript ynvS = new YNVScript();
		switch (linkBhv) 
		{
			case LINKBEHAVIOR.AUTO:
				ynvS.cmd = COMMAND.RUN_AUTO_LINK;
				break;
			case LINKBEHAVIOR.MENU:
				ynvS.cmd = COMMAND.RUN_MENU_LINK;
				break;
			case LINKBEHAVIOR.ENDING:
				ynvS.cmd = COMMAND.RUN_ENDING;
				break;
		}

		scriptList.Add(ynvS);
	}

	void DetectClick()
	{
		if ( !(index < scriptList.Count)) 
		{
			return;
		}
		

		YNVScript sc = scriptList [index];
		switch (sc.cmd) 
		{
			case COMMAND.WAIT_CLICK:
				NextProc();
				break;
		}
	}

	string ConvertADBPathToResources( string str)
	{
		return str.Replace("Assets/Resources/", "").Replace(".ogg", "").Replace(".wav", "").Replace(".mp3", "");
	}

	string ConvertSpritePathForResources( string str)
	{
		return str.Replace("Assets/Resources/", "").Replace(".jpg", "").Replace(".bmp", "").Replace(".png", "");
	}

	void PushLinkButton( string str)
	{
		int num = int.Parse (str);

		// リンクを飛ぶ.
		int i = 1;
		foreach (YNV_Link link in nowItem.link) 
		{
			if( ChkAvailableLink( link))
			{
				if( num == i)
				{
					// 次に飛ぶリンクの決定と関数の脱出.
					nowItem = link.linkedItem;
					ScriptParser ( nowItem.script);
					AddLinkDataToScript (nowItem.linkBehavior);
					break;
				}
				i++;
			}
		}

		// 選択キャンバスの削除.
		Destroy (buttonsCanvas);

	}

	// キャラクターを表示させるときの処理
	void PutCharactor( string charName, string imagePath, int vert, int hori)
	{

		HORIZONTAL horizontal = (HORIZONTAL)hori;

		// データの投入.
		Charactor chara = new Charactor ();
		chara.vertical = (VERTICAL)vert;
		chara.horizontal = horizontal;
		chara.name = charName;


		// 画像位置を代入.
		Image canvasImage = GameObject.Find( horizontal.ToString() + "Char").GetComponent<Image>();
		chara.image = canvasImage;
		canvasImage.enabled = true;

		string path = ConvertSpritePathForResources( imagePath);
		Sprite charaSprite = Resources.Load<Sprite>( path);
		canvasImage.sprite = charaSprite;

		CharaList.Add (chara);

		NextProc();
	}

	// 全てのキャラクターを非表示に
	void RemoveAllCharactor()
	{
		foreach (Charactor c in CharaList) 
		{
			c.image.enabled = false;
		}

		CharaList.Clear ();
	}

	// キャラクターを非表示に
	void RemoveCharactor( string charName)
	{
		Charactor chara = CharaList
							.Where (c => c.name == charName)
							.FirstOrDefault();

		chara.image.enabled = false; // 非表示化
		CharaList.Remove (chara);
	}

	// フェードで画像を出すときの処理
	void BackgroundFadeIn( string path)
	{

		float waitTime = 2F;
		if( waitTime < time)
		{	
			backgroundBK.sprite = backgroundFW.sprite;
			backgroundFW.color = new Color( 1F, 1F, 1F, 1F);
			NextProc();
		}
		else if( isFirstRun()) // 最初のロードの時の処理.
		{
			string spritePath = ConvertSpritePathForResources(path);
			backgroundFW.sprite = Resources.Load<Sprite>( spritePath);
			backgroundFW.color = backgroundBK.color;
		}	
		else
		{
			backgroundFW.color = new Color( 1F, 1F, 1F, time / waitTime);
		}
	}

	bool isFirstRun()
	{
		return time <= 0F;
	}
}
