﻿using UnityEngine;
using UnityEditor;
using System.Collections;

/* */
// それぞれのアイテムを編集する画面です.
/* */

public class YNVEditItemWindow : EditorWindow
{

	[MenuItem("Window/YNovelNeo Item Editor")]
	public static void Open()
	{
		EditorWindow.GetWindow<YNVEditItemWindow>("YNovelNeo Item Editor");
	}


	int popup = 0;
	static TextEditor editor = null;
	
	void OnGUI()
	{
		const float textAreaPosH = 60F;
		wantsMouseMove = true;

		// 選択されたアイテムが存在しているか.
		if (YNovelCore.selectedItem == null) 
		{
			GUILayout.Label ("アイテムが選択されていません。");
		}
		else
		{
			GUILayout.Label ("アイテム名");
			YNovelCore.selectedItem.iName = GUILayout.TextField (YNovelCore.selectedItem.iName);

			// コマンド入力プルダウンメニューを表示.
			popup = EditorGUILayout.Popup ("コマンド入力", popup, new string[] {
				"選択…",
				"クリックを待つ",
				"改ページ",
				"時間待ち",
				"グラフィック読み込み",
				"グラフィック塗りつぶし",
				"キャラクター表示",
				"キャラクターを消す",
				"キャラクターをすべて消す",
				"ウィンドウ表示",
				"ウィンドウを消す",
				"BGM再生",
				"BGMストップ",
				"BGMフェードアウト",
				"効果音再生",
				"効果音ストップ",
				"カウンタをセット",
				"カウンタを増減させる",
				"カウンタをランダムにセット",
				"カウンタをランダムに増減させる"
			});
			if (popup != 0) {
				InputCommand (popup);
				popup = 0;
			}

			const float TEXTAREA_HEIGHT = 200;
			YNovelCore.selectedItem.script = GUI.TextArea (	new Rect (8, textAreaPosH, EditorGUIUtility.currentViewWidth - 16, TEXTAREA_HEIGHT)
			                                               	, YNovelCore.selectedItem.script
			                                               );

			GUILayout.Space (TEXTAREA_HEIGHT + 20);

			BatchPutLinkingGUI ();
		}

		SaveEditorInfo ();

		Repaint ();
	}

	// コマンドをエディタに挿入します.
	void InputCommand( int commandNo)
	{
		COMMAND comID = (COMMAND)commandNo;
		string command = "";

		switch( comID)
		{
		case COMMAND.WAIT_CLICK:
			command = "@";
			break;
		case COMMAND.NEWPAGE:
			command = ".NextPage";
			break;
		case COMMAND.WAIT:
			YNV_WaitTime.Open();
			return;
		case COMMAND.LOAD_GRAPHIC:
			YNV_LoadImageDialog.Open();
			break;
		case COMMAND.FILL_GRAPHIC:
			YNV_FillGraphic.Open();
			return;
		case COMMAND.PUT_CHARACTOR:
			YNV_CharSetDialog.Open();
			break;
		case COMMAND.REMOVE_CHARA:
			YNV_CharaDelete.Open();
			return;
		case COMMAND.REMOVE_ALL_CHARA:
			command = ".CharClear";
			break;
		case COMMAND.PLAY_BGM:
			YNV_PlayBGMDialog.Open();
			break;
		case COMMAND.STOP_BGM:
			command = ".BGMStop";
			break;
		case COMMAND.FADEOUT_BGM:
			YNV_FadeOutBGM.Open();
			return;
		case COMMAND.PLAY_SE:
			YNV_PlaySEDialog.Open();
			break;
		case COMMAND.STOP_SE:
			command = ".SoundStop";
			break;
		case COMMAND.SET_COUNTER:
			YNVSetCounterDialog.Open();
			break;
		case COMMAND.ADD_COUNTER:
			YNVAddSubCounterDialog.Open();
			break;
		case COMMAND.RANDOM_COUNTER:
			YNVSetRandCounterDialog.Open();
			break;
		case COMMAND.ADD_RAND_COUNTER:
			YNVAddSubRandCounterDialog.Open();
			break;
		}

		command = "\n" + command;

		InsertStringForTextarea(command);
	}

	void SaveEditorInfo()
	{
		//backPopUP = popup;
		TextEditor ed = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);

		if( ed.cursorIndex != 0)
		{
			editor = ed;
		}
	}


	// テキストエリアに文字列 str を 挿入します.
	static public void InsertStringForTextarea( string str)
	{
		if (editor == null) 
		{
			return;
		}
		YNovelCore.selectedItem.script = YNovelCore.selectedItem.script.Insert(editor.cursorIndex, str);
	}

	// リンクリストのまとめ描画
	void BatchPutLinkingGUI()
	{
		EditorGUILayout.LabelField ("リンク一覧");


		YNovelCore.selectedItem.linkBehavior = (LINKBEHAVIOR)EditorGUILayout.Popup ("リンク設定", (int)(YNovelCore.selectedItem.linkBehavior), new string[] {
			"メニューリンク",
			"自動リンク",
			"エンディング"
		});

		// 最初の入れ子
		EditorGUILayout.BeginVertical( GUI.skin.box );
		{
			// トップの説明書きの部分 
			EditorGUILayout.BeginHorizontal( GUI.skin.box );
			{
				
				EditorGUILayout.LabelField("有効");
				
				EditorGUILayout.LabelField("テキスト");

				EditorGUILayout.LabelField("リンク先アイテム");

				EditorGUILayout.LabelField("条件");
			}
			EditorGUILayout.EndHorizontal();

			int i = 1;

			// リンクの数に応じて表示する.
			foreach( YNV_Link ynvlink in YNovelCore.selectedItem.link)
			{
				PutLinkingGUI( i, ynvlink);
				i++;
			}

		}

		if (GUILayout.Button ("リンク先追加")) 
		{
			YNovelCore.AddNewLinkToSelectedItem();
		}

		if (GUILayout.Button ("編集")) 
		{
			YNV_EditLink.Open();
		}

		EditorGUILayout.EndVertical();

	}

	// アイテム編集画面下部のテキストフィールドを作成
	void PutLinkingGUI( int num, YNV_Link ynvlink)
	{

		EditorGUILayout.BeginHorizontal( GUI.skin.box );
		{
			ynvlink.enabled = GUILayout.Toggle( ynvlink.enabled, num.ToString());

			GUILayout.TextField( ynvlink.text );

			// リンクアイテムがあるかチェック...
			string linkedItemName = "";
			YNV_Item linkedItem = ynvlink.linkedItem;
			if( linkedItem != null)
			{
				linkedItemName = linkedItem.iName;
			}
			GUILayout.TextField( linkedItemName );

			string condtionStr = "";

			if( ynvlink.useCondition)
			{
				YNV_Condition cond = ynvlink.condition;
				condtionStr = cond.counter.name + "(カウンタ) ≧ " + cond.moreThanNum ;
			}
			GUILayout.TextField( condtionStr );
		}
		EditorGUILayout.EndHorizontal();
	}

}
