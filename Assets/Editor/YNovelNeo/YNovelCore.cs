﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class YNovelCore : ScriptableObject
{
	static public List<YNV_Item> ynvItemList; 
	static public List<YNV_Counter> ynvCounterList;
	static public YNV_Item selectedItem = null;


	public static void Init()
	{
		if( ynvItemList == null)
			ynvItemList = new List<YNV_Item>();
		if (ynvCounterList == null)
			ynvCounterList = new List<YNV_Counter> ();

		selectedItem = null;
	}


	// セーブデータからScriptableObjectを読み込み, 復帰する.
	public static void FirstLoadItemData()
	{
		YNV_SaveData saveData = YNV_SaveData.LoadData("");

		// もしデータが格納されていれば, ロードしない.
		if ( ynvItemList.Count > 0 || saveData == null) 
		{
			return;
		}

		ynvItemList.Clear ();
		ynvItemList = saveData.itemList;
	}

	public static void FirstLoadCounterData()
	{
		YNV_SaveData saveData = YNV_SaveData.LoadData("");

		// もしデータが格納されていれば, ロードしない.
		if ( ynvCounterList.Count > 0 || saveData == null) 
		{
			return;
		}

		ynvCounterList.Clear ();
		ynvCounterList = saveData.counterList;
	}
	

	// アイテムリスト関係の処理.

	// ynvItemListのリスト名を配列で返します.
	static public string[] GetItemListNameByArray( bool usePrefix)
	{
		List<string> str = new List<string>();

		int i = 1;
		foreach (YNV_Item item in ynvItemList) 
		{
			string nameStr = "";

			if( usePrefix)
			{
				nameStr = i.ToString() + ". " + item.iName;
			}
			else
			{
				nameStr = item.iName;
			}

			str.Add( nameStr);

			i++;
		}

		return str.ToArray ();
	}

	static public YNV_Item GetYNV_ItemByName( string str)
	{
		return ynvItemList
				.Where (item => item.iName == str)
				.FirstOrDefault ();
	}

	static public void UpdateLinkData( int index, YNV_Link ynvlink)
	{
		selectedItem.link [index] = ynvlink; 
	}

	static public void AddNewLinkToSelectedItem()
	{
		YNV_Link link = new YNV_Link();
		selectedItem.link.Add(link);
	}
	
	// カウンタ関係の関数郡

	static public void MakeNewCounter()
	{
		ynvCounterList.Add( new YNV_Counter());
	}

	static public void RemoveCounter( int index)
	{
		ynvCounterList.RemoveAt (index);
	}

	static public string[] GetCounterListNameByArray()
	{
		List<string> str = new List<string>();
		int i = 0;

		foreach (YNV_Counter counter in ynvCounterList) 
		{
			string counterStr = i.ToString() + ". " + counter.name;
			str.Add( counterStr);
			i++;
		}

		return str.ToArray ();
	}

	static public YNV_Counter GetYNV_CounterByName( string str)
	{
		return ynvCounterList
				.Where (counter => counter.name == str)
				.FirstOrDefault ();
	}
}
