﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNV_DialogWindow : EditorWindow
{
	protected void PutSpaceAndHL()
	{
		GUILayout.Space (10);
		HorizontalLine ();
		GUILayout.Space (10);
	}

	protected void HorizontalLine()
	{
		GUILayout.Box("", GUILayout.Width(this.position.width), GUILayout.Height(1));
	}

	protected void PutCounterPopup( ref int nowCounter)
	{
		string[] counterListName = YNovelCore.GetCounterListNameByArray ();
		nowCounter = EditorGUILayout.Popup ("編集中カウンタ", nowCounter, counterListName);
	}

	// 第一引数の数字が選択カウンタの最大最初に入っているかチェック.
	protected void ClampNumUsingCounter( ref int num, int nowCounter)
	{
		if (YNovelCore.ynvCounterList.Count <= 0)
			return;
		
		YNV_Counter counter = YNovelCore.ynvCounterList[nowCounter];
		num = Mathf.Clamp ( num, counter.min, counter.max);
	}
}
