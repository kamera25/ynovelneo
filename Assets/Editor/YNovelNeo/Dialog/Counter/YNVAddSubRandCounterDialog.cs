﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNVAddSubRandCounterDialog : YNV_DialogWindow
{
	int nowCounter = 0;
	int minNum = 0;
	int maxNum = 0;
	
	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNVAddSubRandCounterDialog> (true, "ランダム値を増減する", false); 
	}
	
	void OnGUI()
	{
		
		PutCounterPopup ( ref nowCounter);
		
		EditorGUILayout.LabelField ("を");
		
		minNum = EditorGUILayout.IntField ( minNum);
		ClampNumUsingCounter (ref minNum, nowCounter);
		
		EditorGUILayout.LabelField ("から");
		
		maxNum = EditorGUILayout.IntField ( maxNum);
		ClampNumUsingCounter (ref maxNum, nowCounter);
		
		EditorGUILayout.LabelField ("の間のランダムな数を足す。");
		
		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK") && YNovelCore.ynvCounterList.Count > 0) 
		{
			string counterName = YNovelCore.ynvCounterList[nowCounter].name;
			
			YNVEditItemWindow.InsertStringForTextarea( ".CounterRndPlus \"" + counterName + "\" " + minNum + " " + maxNum);
			Close();
		}
	}
	
}
