﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNVSetCounterDialog : YNV_DialogWindow
{
	int nowCounter = 0;
	int num = 0;

	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNVSetCounterDialog> (true, "カウンタをセットする", false); 
	}
	
	void OnGUI()
	{
		
		PutCounterPopup ( ref nowCounter);

		EditorGUILayout.LabelField ("を");

		num = EditorGUILayout.IntField ( num);
		ClampNumUsingCounter (ref num, nowCounter);

		EditorGUILayout.LabelField ("にセットする。");
		
		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK") && YNovelCore.ynvCounterList.Count > 0) 
		{
			string counterName = YNovelCore.ynvCounterList[nowCounter].name;

			YNVEditItemWindow.InsertStringForTextarea( ".CounterSet \"" + counterName + "\" " + num);
			Close();
		}
	}


}
