﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNVSetRandCounterDialog : YNV_DialogWindow
{
	int nowCounter = 0;
	int minNum = 0;
	int maxNum = 0;

	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNVSetRandCounterDialog> (true, "ランダムにセットする", false); 
	}
	
	void OnGUI()
	{
		
		PutCounterPopup ( ref nowCounter);
		
		EditorGUILayout.LabelField ("を");
		
		minNum = EditorGUILayout.IntField ( minNum);
		ClampNumUsingCounter (ref minNum, nowCounter);

		EditorGUILayout.LabelField ("から");
		
		maxNum = EditorGUILayout.IntField ( maxNum);
		ClampNumUsingCounter (ref maxNum, nowCounter);

		EditorGUILayout.LabelField ("の間のランダムな数をセットする。");
		
		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK") && YNovelCore.ynvCounterList.Count > 0) 
		{
			string counterName = YNovelCore.ynvCounterList[nowCounter].name;
			
			YNVEditItemWindow.InsertStringForTextarea( ".CounterRndSet \"" + counterName + "\" " + minNum + " " + maxNum);
			Close();
		}
	}

}
