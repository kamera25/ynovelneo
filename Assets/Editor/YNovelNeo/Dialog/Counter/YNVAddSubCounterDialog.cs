﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNVAddSubCounterDialog : YNV_DialogWindow
{
	int nowCounter = 0;
	int num = 0;
	
	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNVAddSubCounterDialog> (true, "カウンタを増減させる", false); 
	}
	
	void OnGUI()
	{
		
		PutCounterPopup ( ref nowCounter);
		
		EditorGUILayout.LabelField ("に");
		
		num = EditorGUILayout.IntField ( num);
		
		EditorGUILayout.LabelField ("を足す。");
		
		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK") && YNovelCore.ynvCounterList.Count > 0) 
		{
			string counterName = YNovelCore.ynvCounterList[nowCounter].name;
			
			YNVEditItemWindow.InsertStringForTextarea( ".CounterPlus \"" + counterName + "\" " + num);
			Close();
		}
	}
}
