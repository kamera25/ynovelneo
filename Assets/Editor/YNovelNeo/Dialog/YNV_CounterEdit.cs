﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNV_CounterEdit : YNV_DialogWindow
{
	int nowCounter = 0;
	int backMaxNum = 0;

	[MenuItem("Window/YNovelNeo Counter Setting")]
	static void Open()
	{
		EditorWindow.GetWindow<YNV_CounterEdit>("YNovelNeo Counter Setting");
	}

	//一番上のバー表示を描画する.
	void DrawTopBar()
	{
		EditorGUILayout.BeginHorizontal( GUI.skin.box );
		{
			
			if( GUILayout.Button("新規" ) )
			{
				YNovelCore.MakeNewCounter();
				nowCounter = YNovelCore.ynvCounterList.Count - 1;
			}
			
			if( GUILayout.Button("削除" ) )
			{
				if( YNovelCore.ynvCounterList.Count > 0)
				{
					YNovelCore.RemoveCounter( nowCounter);
					nowCounter = 0;
				}
			}

		}
		EditorGUILayout.EndHorizontal();
	}

	void OnGUI()
	{

		DrawTopBar ();

		PutCounterPopup ( ref nowCounter);

		if (YNovelCore.ynvCounterList.Count > 0 && nowCounter >= 0)
		{
			YNV_Counter ynvCounter = YNovelCore.ynvCounterList[nowCounter];

			EditorGUILayout.BeginVertical (GUI.skin.box);
			{
				ynvCounter.name = EditorGUILayout.TextField ("カウンタ名", ynvCounter.name);

				ynvCounter.defaultNum = EditorGUILayout.IntField ("ゲーム開始時の初期値", ynvCounter.defaultNum);

				ynvCounter.max = EditorGUILayout.IntField ("最大値", ynvCounter.max);

				ynvCounter.min = EditorGUILayout.IntField ("最小値", ynvCounter.min);

				ynvCounter.carryOver = EditorGUILayout.Toggle ("ゲームクリア後も値を引き継ぐか", ynvCounter.carryOver);

				EditorGUILayout.Space ();
			}
			EditorGUILayout.EndVertical ();

			CheckValidate ( ynvCounter);
		}


	}

	// 入力された変数に正当性があるかチェックします.
	void CheckValidate( YNV_Counter ynvCounter)
	{
		// 上限内に初期値はあるか.
		ynvCounter.defaultNum = Mathf.Clamp (ynvCounter.defaultNum, ynvCounter.min, ynvCounter.max);

		if ( !(ynvCounter.min < ynvCounter.max)) 
		{
			// 最大値が変更されたなら.
			if( backMaxNum != ynvCounter.max)
			{
				ynvCounter.max = ynvCounter.min;
			}
			else //最小値が変更されたら.
			{
				ynvCounter.min = ynvCounter.max;
			}
		}

		backMaxNum = ynvCounter.max;
	}
}
