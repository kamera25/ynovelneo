﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNV_LoadImageDialog : YNV_DialogWindow
{
	
	Sprite image = null;
	
	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNV_LoadImageDialog> (true, "画像の読み込み", false); 
	}
	
	void OnGUI()
	{
		
		image = (Sprite) EditorGUILayout.ObjectField ("画像ファイル", image, typeof(Sprite), false);
		
		
		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK")) 
		{	
			YNVEditItemWindow.InsertStringForTextarea( ".GraphLoad \"" + AssetDatabase.GetAssetPath(image) +"\"");
			Close();
		}
	}
}
