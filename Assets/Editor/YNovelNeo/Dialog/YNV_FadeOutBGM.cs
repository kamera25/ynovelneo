using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNV_FadeOutBGM : YNV_DialogWindow
{

	int fadeOutTime = 2000;

	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNV_FadeOutBGM> (true, "BGMフェードアウト", false); 
	}

	void OnGUI()
	{
		
		fadeOutTime = EditorGUILayout.IntField( "フェードアウト時間(ミリ秒)", fadeOutTime );
		
		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK")) 
		{
			YNVEditItemWindow.InsertStringForTextarea( ".BGMFadeout " + fadeOutTime);
			Close();
		}
	}
	
}
