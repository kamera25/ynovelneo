﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class YNV_EditLink : YNV_DialogWindow
{
	
	// Dialog内で使う変数
	int editItem = 0;
	string text = "";
	int link = 0;
	bool useCounter = false;
	int counter = 0;
	int conditionNum = 0;

	int backEditItem = 0;

	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNV_EditLink> (true, "リンク編集", false); 
	}

	void OnGUI()
	{

		editItem = EditorGUILayout.Popup( "編集アイテム", editItem, MakeEditItemNumberAsArray() );

		if (editItem == 0) 
		{
			return;
		} 
		else if( backEditItem != editItem)
		{
			UpdateData();
		}


		PutSpaceAndHL ();

		text = EditorGUILayout.TextField( "テキスト", text );

		PutSpaceAndHL ();


		link = EditorGUILayout.Popup( "リンク先", link, YNovelCore.GetItemListNameByArray(true) );

		PutSpaceAndHL ();

		EditorGUILayout.LabelField( "リンク条件" );

		useCounter = EditorGUILayout.BeginToggleGroup ("カウンタを有効", useCounter);
			// 条件付き分岐があるなら
			EditorGUILayout.BeginVertical( GUI.skin.box );
			{
				counter = EditorGUILayout.Popup( "", counter, YNovelCore.GetCounterListNameByArray() );

				EditorGUILayout.LabelField( "が" );

				conditionNum = EditorGUILayout.IntField( "", conditionNum );

				EditorGUILayout.LabelField( "以上" );
			}
			EditorGUILayout.EndVertical();
		EditorGUILayout.EndToggleGroup();

		GUILayout.Space(20);

		if (GUILayout.Button ("保存")) 
		{
			SaveLinkData();
		}

		// 次回ループ用の処理
		backEditItem = editItem;
	}

	// 書き換えられた情報をセーブします
	void SaveLinkData()
	{
		YNV_Link ynvLink = new YNV_Link ();
		int index = editItem - 1;

		ynvLink.text = text;

		// リンクするアイテムを格納.
		ynvLink.linkedItem = YNovelCore.ynvItemList[link];

		// 条件が有効なら.
		if (useCounter) 
		{
			YNV_Condition cond = new YNV_Condition ();
			cond.counter = YNovelCore.ynvCounterList [counter];
			cond.moreThanNum = conditionNum;

			ynvLink.condition = cond;
			ynvLink.useCondition = true;
		} else 
		{
			ynvLink.useCondition = false;
			ynvLink.condition = null;
		}

		// アップデートをかける.
		YNovelCore.UpdateLinkData (index, ynvLink);

		Close ();
	}
	
	string[] MakeEditItemNumberAsArray()
	{
		List<string> editItemArray = new List<string>();

		editItemArray.Add ("選択…");
		for(int i = 0; i < YNovelCore.selectedItem.link.Count; i++)
		{
			editItemArray.Add( (i+1).ToString());
		}

		return editItemArray.ToArray();
	}

	// Linkインスタンスからデータを取って来る
	void UpdateData()
	{
		YNV_Link ynvNowLink = YNovelCore.selectedItem.link[editItem-1];

		text = ynvNowLink.text;

		link = YNovelCore.ynvItemList
				.Select((item, index) => new { Index = index, Value = item})
				.Where( item => item.Value == ynvNowLink.linkedItem)
				.Select( item => item.Index)
				.FirstOrDefault();

		if (ynvNowLink.useCondition == true) {
			useCounter = true;
			counter = YNovelCore.ynvCounterList
					.Select((item, index) => new { Index = index, Value = item})
					.Where( item => item.Value == ynvNowLink.condition.counter)
					.Select( item => item.Index)
					.FirstOrDefault();
			conditionNum = ynvNowLink.condition.moreThanNum;
		}
	}

}
