﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNV_PlayBGMDialog : YNV_DialogWindow
{

	AudioClip audio = null;
	
	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNV_PlayBGMDialog> (true, "BGM再生", false); 
	}
	
	void OnGUI()
	{
		
		audio = (AudioClip)EditorGUILayout.ObjectField ("BGMファイル", audio, typeof(AudioClip), false);
		
		
		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK")) 
		{	
			YNVEditItemWindow.InsertStringForTextarea( ".BGMPlay \"" + AssetDatabase.GetAssetPath(audio) +"\"");
			Close();
		}
	}
}
