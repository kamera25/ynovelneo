﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNV_PlaySEDialog : YNV_DialogWindow
{
	
	AudioClip audio = null;
	
	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNV_PlaySEDialog> (true, "効果音再生", false); 
	}
	
	void OnGUI()
	{
		
		audio = (AudioClip)EditorGUILayout.ObjectField ("効果音ファイル", audio, typeof(AudioClip), false);
		
		
		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK")) 
		{	
			YNVEditItemWindow.InsertStringForTextarea( ".SoundPlay \"" + AssetDatabase.GetAssetPath(audio) +"\"");
			Close();
		}
	}
}