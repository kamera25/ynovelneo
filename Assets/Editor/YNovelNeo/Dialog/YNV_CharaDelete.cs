using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNV_CharaDelete : YNV_DialogWindow
{
	
	string deleteCharaName = "";
	
	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNV_CharaDelete> (true, "キャラクターを消す", false); 
	}
	
	void OnGUI()
	{
		
		deleteCharaName = EditorGUILayout.TextField( "キャラクター名", deleteCharaName );
		
		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK")) 
		{
			YNVEditItemWindow.InsertStringForTextarea( ".CharDelete \"" + deleteCharaName + "\"");
			Close();
		}
	}

}
