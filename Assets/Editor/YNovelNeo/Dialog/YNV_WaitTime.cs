using UnityEngine;
using UnityEditor;
using System.Collections;

// 待ち時間を入力するダイアログ
public class YNV_WaitTime : YNV_DialogWindow
{
	int waitTime = 1000;


	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNV_WaitTime> (true, "時間待ち", false); 
	}

	void OnGUI()
	{

		waitTime = EditorGUILayout.IntField( "待ち時間(ミリ秒)", waitTime );

		PutSpaceAndHL ();

		if (GUILayout.Button ("OK")) 
		{
			YNVEditItemWindow.InsertStringForTextarea( ".Wait " + waitTime);
			Close();
		}
	}


}
