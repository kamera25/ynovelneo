﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNV_CharSetDialog : YNV_DialogWindow
{
	string charaName = "";
	Sprite image = null;

	HORIZONTAL horizontal = HORIZONTAL.CENTER;
	VERTICAL vertical = VERTICAL.MIDDLE;
	
	public static void Open()
	{
		// 小窓の作成
		EditorWindow.GetWindow<YNV_CharSetDialog> (true, "キャラクター表示", false); 
	}
	
	void OnGUI()
	{
		charaName = EditorGUILayout.TextField( "キャラクター名", charaName );

		PutSpaceAndHL ();

		image = (Sprite) EditorGUILayout.ObjectField ("画像ファイル", image, typeof(Sprite), false);

		PutSpaceAndHL ();

		EditorGUILayout.LabelField( "グラフィックの位置" );

		horizontal = (HORIZONTAL)EditorGUILayout.Popup ("水平方向", (int)horizontal, new string[] {
			"右",
			"中央",
			"左"
		});
		vertical = (VERTICAL) EditorGUILayout.Popup ("垂直方向", (int)vertical, new string[] {
			"上",
			"中央",
			"下"
		});

		PutSpaceAndHL ();
		
		if (GUILayout.Button ("OK") && charaName != "" && image != null) 
		{	
			YNVEditItemWindow.InsertStringForTextarea( ".CharSet \"" + charaName + "\" \""+ AssetDatabase.GetAssetPath(image) +"\" " + vertical + " " + horizontal );
			Close();
		}
	}
}