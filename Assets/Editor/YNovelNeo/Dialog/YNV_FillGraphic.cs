using UnityEngine;
using UnityEditor;
using System.Collections;

public class YNV_FillGraphic : YNV_DialogWindow
{
	Color color = Color.white;
	int fadeStyle = 0;

	static public void Open()
	{
		EditorWindow.GetWindow<YNV_FillGraphic>(true, "グラフィック塗りつぶし", false);
	}

	void OnGUI()
	{
		EditorGUILayout.LabelField ("塗りつぶす色");
		color = EditorGUILayout.ColorField( color);

		PutSpaceAndHL ();

		EditorGUILayout.LabelField ("フェード効果");
		fadeStyle = EditorGUILayout.Popup ("コマンド入力", fadeStyle, new string[] {
			"標準フェード",
		});

		PutSpaceAndHL ();

		if (GUILayout.Button ("OK")) 
		{
			SendGraphFill();
			Close();
		}
	}

	void SendGraphFill()
	{
		YNVEditItemWindow.InsertStringForTextarea( ".GraphFill " 
		                                       + (int)(color.r * 255) 
		                                       + " " 
		                                       + (int)(color.g * 255) 
		                                       + " " 
		                                       + (int)(color.b * 255) 
		                                       + " " 
		                                       + (int)(color.a * 255)
		);


	}
}
