using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



public class YNVChart : EditorWindow
{

	EventType eType;
	YNV_Item draggedItem;
	bool useMouse = false;

	float hScrollVal = 0F;
	float vScrollVal = 0F;

	[MenuItem("Window/YNovelNeo Chart")]
	static void Init ()
	{
		EditorWindow.GetWindow<YNVChart> ("YNovelNeo Chart");
	}

	void OnEnable()
	{
		YNovelCore.Init ();
		YNovelCore.FirstLoadItemData ();
		YNovelCore.FirstLoadCounterData ();

		draggedItem = null;
	}

	//一番上のバー表示を描画する.
	void DrawTopBar()
	{
		EditorGUILayout.BeginHorizontal( GUI.skin.box );
		{
			
			EditorGUILayout.PrefixLabel( "チャート操作" );
			
			if( GUILayout.Button("新規アイテム" ) )
			{
				YNV_Item ynvItem = new YNV_Item();
				YNovelCore.ynvItemList.Add( ynvItem);
			}
			
			if( GUILayout.Button("削除" ) )
			{
				DeleteSelectItem();
			}
			
			if( GUILayout.Button("保存" ) )
			{
				YNV_SaveData.SaveData( YNovelCore.ynvItemList, YNovelCore.ynvCounterList);
			}

		}
		EditorGUILayout.EndHorizontal();
	}

	// YNV_Itemインスタンスからボタンを一つ表示する
	void DrawButton( YNV_Item item)
	{
		GUIStyle nodeStyle = null;

		// リンク振る舞いによってブロックの色を変更する
		switch (item.linkBehavior) 
		{
			case LINKBEHAVIOR.MENU:
				nodeStyle = (item.selected)?(GUIStyle)"flow node 1 on" : (GUIStyle)"flow node 1";
				break;
			case LINKBEHAVIOR.AUTO:
				nodeStyle = (item.selected)?(GUIStyle)"flow node 2 on" : (GUIStyle)"flow node 2";
				break;
			case LINKBEHAVIOR.ENDING:
				nodeStyle = (item.selected)?(GUIStyle)"flow node 6 on" : (GUIStyle)"flow node 6";
				break;
		}

		// 文字は中央揃え
		nodeStyle.alignment = TextAnchor.MiddleCenter;

		item.push = GUI.RepeatButton ( new Rect 
		                            (
											item.pos.x - hScrollVal
		 								, 	item.pos.y - vScrollVal
		 								, 	item.size.x
										, 	item.size.y
									)
		                            , item.iName
		                            , nodeStyle);

		if (item.push) {
			BatchUnSelect();
			useMouse = true;
			item.selected = true;
		}
	}

	// アイテム群を全て描画する
	void BatchDrawButton()
	{
		useMouse = false;
		foreach( YNV_Item item in YNovelCore.ynvItemList)
		{
			DrawButton( item);
		}
	}

	void BatchUnSelect()
	{
		foreach( YNV_Item item in YNovelCore.ynvItemList)
		{
			item.selected = false;
		}
	}

	// 選択されたアイテムを削除します
	void DeleteSelectItem()
	{
		YNV_Item delItem = YNovelCore.ynvItemList
							.Where (item => item.selected == true)
							.FirstOrDefault ();
		YNovelCore.ynvItemList.Remove (delItem);
	}

	void MoveItemPos()
	{

		// ドラッグしている アイテムノード があれば.
		if (draggedItem == null) 
		{

			if (eType != EventType.mouseDrag && !useMouse)
				return;

			// アイテムで 押下フラグがついているのを探す.
			foreach (YNV_Item item in YNovelCore.ynvItemList) 
			{
				if (item.push) 
				{
					draggedItem = item;
				}
			}	
		} 
		else 
		{
			if( eType == EventType.MouseUp)
			{
				YNovelCore.selectedItem = draggedItem;
				draggedItem = null;
			}
			else
			{
				Vector2 newpos = Event.current.mousePosition;
				newpos.x -= draggedItem.size.x / 2 - hScrollVal;
				newpos.y -= draggedItem.size.y / 2 - vScrollVal;
				draggedItem.pos = newpos;
			}
		}


	}

	void OnGUI()
	{
		wantsMouseMove = true;
		eType = Event.current.type;

		BatchDrawBezierLink ();

		BatchDrawButton ();

		DrawTopBar ();

		MoveItemPos ();

		ScrollViewControl ();


		Repaint ();
	}

	private void ScrollViewControl()
	{
		const float palletSize = 400F;
		float moveAccelerationX = palletSize / Screen.width;
		float moveAccelerationY = palletSize / Screen.height;

		// 水平スクロールバー
		Rect hScroll = new Rect ( 0F, Screen.height - 40F, Screen.width, 10F);
		hScrollVal = GUI.HorizontalScrollbar ( hScroll, hScrollVal, 10F * moveAccelerationX, 0F, palletSize);

		// 垂直スクロールバー
		float topBarHeight = 30F;
		Rect wScroll = new Rect ( Screen.width - 15F, topBarHeight, 10F, Screen.height - topBarHeight - 40);
		vScrollVal = GUI.VerticalScrollbar ( wScroll, vScrollVal, 10F * moveAccelerationY, 0F, palletSize);
	}

	public static void SaveDetailFromEditItemWindow( string dname, string script)
	{
		if ( YNovelCore.selectedItem == null) 
		{
			return;
		}

		YNovelCore.selectedItem.iName = dname;
		YNovelCore.selectedItem.script = script;
	}

	// アイテム間のベジェ線をまとめて引く.
	private void BatchDrawBezierLink()
	{
		foreach( YNV_Item ynvItemParent in YNovelCore.ynvItemList)
		{
			foreach( YNV_Link ynvItemLink in ynvItemParent.link)
			{
				YNV_Item ynvItemChild = ynvItemLink.linkedItem;
				if( ynvItemChild != null)
				{
					DrawBezierLink( ynvItemParent, ynvItemChild);
				}
			}
		}
	}

	// アイテム間のベジェ線を引く.
	private void DrawBezierLink( YNV_Item item1, YNV_Item item2)
	{
		Vector3 start	= new Vector3( item1.pos.x + item1.size.x / 2F - hScrollVal
		                            , item1.pos.y + item1.size.y / 2F - vScrollVal
		                            , -20F);
		Vector3 end		= new Vector3( item2.pos.x + item2.size.x / 2F - hScrollVal
		                          , item2.pos.y + item2.size.y / 2F - vScrollVal
		                          , -20F);

		Vector3 tan = (start + end) / 2F;


		Vector3 lookAtVec = end - start;
		if( lookAtVec.sqrMagnitude >= 0F)
		{
			Quaternion Q = new Quaternion();
			Q.SetLookRotation ( lookAtVec);


			Handles.DrawBezier (start
			                    , end
			                    , tan
			                    , tan
			                    , new Color(0.4F, 0.4F, 0.4F, 1F)
			                    , null
			                    , 3F
			                    );

			// 三角矢印を作る
			Handles.color = Color.gray;
			Handles.ConeCap( 1, tan, Q, 15F);
		}
	}
}
