# YNovelNeo 

YNovelNeo はUnityで使えるノベルフレームワークです。  
[YuukiNove](http://freett.com/yuukiex/yuukinovel.html) にインスパイアされています。

## 開発について

このフレームワークは現在開発中で動かないところが多々あります。 
その事をご理解いただける方のみお使いください。

## ライセンス
ライセンスは MITライセンス です。